package org.actora.core;

import org.actora.core.process.Process;
import org.actora.core.process.ProcessCreated;
import org.actora.core.process.ProcessDetails;
import org.actora.core.process.ProcessFactoryException;
import org.emerjoin.event.contract.Emits;

/**
 * Creates new processes
 */
public interface ProcessFactory {

    @Emits(ProcessCreated.class)
    Process create(ProcessDetails details) throws ProcessFactoryException;

}
