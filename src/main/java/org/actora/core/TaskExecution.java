package org.actora.core;


import org.actora.core.process.Process;
import org.actora.core.task.TaskExecutionInterrupted;
import org.actora.core.task.TaskExecutionRequest;
import org.actora.core.task.TaskExecutionStage;
import org.emerjoin.event.contract.Emits;

import java.time.Instant;
import java.util.Optional;

public class TaskExecution {

    private TaskExecutionRequest request;
    private Process process;
    private TaskExecutionStage stage = TaskExecutionStage.REQUESTED;
    private Instant submissionInstant;
    private Instant lastStateChange;
    private boolean failed = false;
    private String errorMessage;

    TaskExecution(TaskExecutionRequest request, Process process){
        this.request = request;
        this.process = process;
        this.submissionInstant = Instant.now();
    }

    public TaskKey getKey(){
        return this.request.getTaskKey();
    }

    public boolean isComplete(){
        return stage == TaskExecutionStage.COMPLETE;
    }

    public boolean hasFailed(){
        return failed;
    }

    public Optional<String> getErrorMessage(){
        return Optional.ofNullable(
                errorMessage);
    }

    public TaskExecutionRequest getRequest() {
        return request;
    }

    public TaskExecutionStage getStage() {
        return stage;
    }

    public Instant getSubmissionInstant() {
        return submissionInstant;
    }

    public Instant getLastStateChange() {
        return lastStateChange;
    }

    @Emits(TaskExecutionInterrupted.class)
    public void interrupt(){

        //TODO: Kill process (If Running Yet)
        throw new UnsupportedOperationException();

    }

    void setFailed(){
        this.failed = true;
    }

    void setFailed(String errorMessage){
        this.failed = true;
        this.errorMessage = errorMessage;
    }

    void setStage(TaskExecutionStage stage){
        this.stage = stage;
        this.lastStateChange = Instant.now();
    }

}
