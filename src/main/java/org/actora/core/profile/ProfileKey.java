package org.actora.core.profile;

public class ProfileKey {

    private String value;

    public ProfileKey(String value){
        if(value==null||value.isEmpty())
            throw new IllegalArgumentException("value must not be null nor empty");
        this.value = value;
    }

    public String getValue(){
        return this.value;
    }

    @Override
    public String toString() {
        return String.format("ProfileKey[%s]",value);
    }
}
