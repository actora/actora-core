package org.actora.core.profile;

import org.actora.core.CoreEvent;

public interface ProfileEvent extends CoreEvent {

    ProfileKey getProfileKey();

}
