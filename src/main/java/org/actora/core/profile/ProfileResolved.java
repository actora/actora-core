package org.actora.core.profile;

public interface ProfileResolved extends ProfileEvent {

    Profile getProfile();

}
