package org.actora.core.profile;

import org.actora.core.ActoraCoreException;

public class ProfileResolverException extends ActoraCoreException {

    public ProfileResolverException(String message) {
        super(message);
    }

    public ProfileResolverException(String message, Throwable cause) {
        super(message, cause);
    }

}
