package org.actora.core.profile;

import java.io.File;

public interface Profile {

    ProfileKey getKey();
    File getPackage();

}
