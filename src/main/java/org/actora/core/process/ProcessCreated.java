package org.actora.core.process;

public interface ProcessCreated extends ProcessEvent {

    Process getProcess();

}
