package org.actora.core.process;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ProcessDetails {

    private File workingDirectory;
    private String[] commands;
    private Map<String,String> environmentVariables = new HashMap<>();

    public ProcessDetails(File workingDirectory, Map<String,String> environmentVariables, String... commands){
        if(workingDirectory==null||!workingDirectory.isDirectory()||!workingDirectory.exists())
            throw new IllegalArgumentException("an existing working directory reference is required");
        if(environmentVariables==null)
            throw new IllegalArgumentException("environment variables must not be null");
        if(commands==null||commands.length==0)
            throw new IllegalArgumentException("at least one command must be provided");
        this.workingDirectory = workingDirectory;
        this.environmentVariables.putAll(environmentVariables);
        this.commands = commands;
    }

    public ProcessDetails(File workingDirectory, String... commands){
        this(workingDirectory, Collections.emptyMap(),commands);
    }

    public File getWorkingDirectory() {
        return workingDirectory;
    }

    public String[] getCommands() {
        return commands;
    }

    public Map<String, String> getEnvironmentVariables() {
        return Collections.unmodifiableMap(environmentVariables);
    }
}
