package org.actora.core.process;

public interface ProcessKilled extends ProcessEvent {

    PID getId();

}
