package org.actora.core.process;

public interface ProcessExitCode {

    int value();
    boolean isError();


}
