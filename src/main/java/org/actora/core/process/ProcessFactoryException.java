package org.actora.core.process;

import org.actora.core.ActoraCoreException;

public class ProcessFactoryException extends ActoraCoreException {

    public ProcessFactoryException(String message) {
        super(message);
    }

    public ProcessFactoryException(String message, Throwable cause) {
        super(message, cause);
    }

}
