package org.actora.core.process;

import org.emerjoin.event.contract.Emits;

import java.time.Duration;
import java.util.Optional;

public interface Process {

    PID getId();
    boolean hasExited();
    Duration getProcessStart();
    Duration getProcessDuration();
    Optional<ProcessExitCode> getExitCode();
    ProcessExitCode waitUntilExits();

    @Emits(ProcessKilled.class)
    boolean kill();

}
