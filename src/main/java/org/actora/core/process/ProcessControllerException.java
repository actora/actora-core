package org.actora.core.process;

import org.actora.core.ActoraCoreException;

public class ProcessControllerException extends ActoraCoreException {

    public ProcessControllerException(String message) {
        super(message);
    }

    public ProcessControllerException(String message, Throwable cause) {
        super(message, cause);
    }

}
