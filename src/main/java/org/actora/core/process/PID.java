package org.actora.core.process;

/**
 * Represents a process ID
 */
public class PID {

    private String value;

    public PID(String value){
        if(value==null||value.isEmpty())
            throw new IllegalArgumentException("value must not be null nor empty");
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
