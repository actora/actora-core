package org.actora.core.task;

import org.actora.core.CoreEvent;
import org.actora.core.TaskExecution;
import org.actora.core.TaskKey;

public interface TaskExecutionEvent extends CoreEvent {

    TaskKey getTaskKey();
    TaskExecution getExecution();

}
