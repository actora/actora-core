package org.actora.core.task;

public enum TaskExecutionStage {
    REQUESTED, PREPARING, STARTING, EXECUTING, COMPLETE
}