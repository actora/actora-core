package org.actora.core.task;

import org.actora.core.TaskKey;
import org.actora.core.profile.ProfileKey;

import java.util.Objects;

public class TaskExecutionRequest {

    private TaskKey taskKey;
    private ProfileKey profileKey;

    public TaskExecutionRequest(TaskKey taskKey, ProfileKey profileKey){
        this.taskKey = Objects.requireNonNull(taskKey,"taskKey must not be null");
        this.profileKey = Objects.requireNonNull(profileKey,"profileKey must not be null");
    }

    public TaskKey getTaskKey() {
        return taskKey;
    }

    public ProfileKey getProfileKey() {
        return profileKey;
    }
}
