package org.actora.core;

public class TaskKey {

    private String ref;
    private String id;

    public TaskKey(String ref, String id){
        if(ref==null||ref.isEmpty())
            throw new IllegalArgumentException("ref must not be null nor empty");
        if(id==null||id.isEmpty())
            throw new IllegalArgumentException("id must not be null nor empty");
        this.ref = ref;
        this.id  = id;
    }

    public String getRef() {
        return ref;
    }

    public String getId() {
        return id;
    }

    public String toString(){
        return String.format("TaskKey[%s:%s]",ref,id);
    }

}
