package org.actora.core;

import org.actora.core.profile.Profile;
import org.actora.core.profile.ProfileKey;
import org.actora.core.profile.ProfileResolved;
import org.actora.core.profile.ProfileResolverException;
import org.emerjoin.event.contract.Emits;

import java.util.Optional;

/**
 * Resolves task execution profiles
 */
public interface ProfileResolver {

    @Emits(ProfileResolved.class)
    Optional<Profile> resolve(ProfileKey key) throws ProfileResolverException;

}
