package org.actora.core.workspace;

import org.actora.core.ActoraCoreException;

public class WorkspaceControllerException extends ActoraCoreException {

    public WorkspaceControllerException(String message) {
        super(message);
    }

    public WorkspaceControllerException(String message, Throwable cause) {
        super(message, cause);
    }

}
