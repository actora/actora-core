package org.actora.core.workspace;

public class WorkspaceKey {

    private String value;

    public WorkspaceKey(String value){
        if(value==null||value.isEmpty())
            throw new IllegalArgumentException("value must not be null nor empty");
        this.value = value;
    }

    public String getValue(){
        return this.value;
    }

    @Override
    public String toString() {
        return String.format("WorkspaceKey[%s]",value);
    }
}
