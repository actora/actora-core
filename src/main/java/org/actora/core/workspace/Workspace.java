package org.actora.core.workspace;

import org.emerjoin.event.contract.Emits;

import java.io.File;

public interface Workspace {

    WorkspaceKey getKey();
    @Emits(WorkspaceDestroyed.class)
    void destroy();
    boolean exists();
    File getFile();

}
