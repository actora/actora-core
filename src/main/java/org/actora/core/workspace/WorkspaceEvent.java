package org.actora.core.workspace;

import org.actora.core.CoreEvent;

public interface WorkspaceEvent extends CoreEvent {

    Workspace getWorkspace();

}
