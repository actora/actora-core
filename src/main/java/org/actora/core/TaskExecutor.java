package org.actora.core;


import org.actora.core.task.*;
import org.emerjoin.event.contract.Emits;

import java.util.*;

/**
 * The entry-point of Actora-core. It encapsulates all internal components, exposing a task-execution service API which resolves around {@link TaskExecutionRequest}s and {@link TaskExecution}s.
 */
@Emits(PreparingTaskExecution.class)
@Emits(TaskExecutionStarting.class)
@Emits(TaskExecutionStarted.class)
@Emits(TaskExecutionComplete.class)
@Emits(TaskExecutionFailed.class)
public class TaskExecutor {

    private WorkspaceController workspaceController;
    private ProcessFactory processFactory;
    private ProcessController processController;
    private ProfileResolver profileResolver;

    private Map<String,TaskExecution> taskExecutionMap = Collections.synchronizedMap(new HashMap<>());

    public TaskExecutor(WorkspaceController workspaceController, ProcessFactory processFactory, ProcessController processController, ProfileResolver profileResolver){
        this.workspaceController = Objects.requireNonNull(workspaceController, "workspaceController must not be null");
        this.processFactory = Objects.requireNonNull(processFactory, "processFactory must not be null");
        this.processController = Objects.requireNonNull(processController, "processController must not be null");
        this.profileResolver = Objects.requireNonNull(profileResolver, "profileResolver must not be null");
        this.initialize();
    }

    private void initialize(){
        this.processController.setProcessFactory(processFactory);
    }

    public  List<TaskExecution> listTaskExecutions(){

        //TODO: Implement
        throw new UnsupportedOperationException();

    }

    public Optional<TaskExecution> getExecution(TaskKey taskKey){
        if(taskKey==null)
            throw new IllegalArgumentException("taskKey must not be null");
        return Optional.ofNullable(taskExecutionMap.get(taskKey.
                toString()));
    }

    public void execute(TaskExecutionRequest request){
        if(request==null)
            throw new IllegalArgumentException("request must not be null");

        //TODO: Emit: Preparing Task Execution
        //TODO: Resolve profile
        //TODO: Make Create Workspace
        //TODO: Emit: Starting Task Execution
        //TODO: Ask process controller to start process
        //TODO: Emit: task execution Started
        //TODO: If process start failed: Emit task execution failed

    }


}
