package org.actora.core;

import org.actora.actora.infrastructure.ActoraEvent;

/**
 * Represents a core event.
 */
public interface CoreEvent extends ActoraEvent {



}
