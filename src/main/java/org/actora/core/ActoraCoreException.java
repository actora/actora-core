package org.actora.core;

public class ActoraCoreException extends RuntimeException {

    public ActoraCoreException(String message){
        super(message);
    }

    public ActoraCoreException(String message, Throwable cause){
        super(message,cause);
    }

}
