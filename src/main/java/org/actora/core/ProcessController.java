package org.actora.core;

import org.actora.core.process.PID;
import org.actora.core.process.Process;
import org.actora.core.process.ProcessControllerException;
import org.actora.core.process.ProcessDetails;

import java.util.List;
import java.util.Optional;

/**
 * Manages de creation and destruction of processes
 */
public interface ProcessController {

    Process startProcess(ProcessDetails processDetails) throws ProcessControllerException;
    void setProcessFactory(ProcessFactory processFactory);
    List<Process> listRunningProcesses() throws ProcessControllerException;
    Optional<PID> getProcess(PID pid) throws ProcessControllerException;

}
