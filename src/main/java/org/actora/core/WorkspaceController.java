package org.actora.core;

import org.actora.core.profile.Profile;
import org.actora.core.workspace.Workspace;
import org.actora.core.workspace.WorkspaceControllerException;
import org.actora.core.workspace.WorkspaceCreated;
import org.actora.core.workspace.WorkspaceKey;
import org.emerjoin.event.contract.Emits;

import java.util.List;
import java.util.Optional;

/**
 * Manages task execution workspaces
 */
public interface WorkspaceController {

    @Emits(WorkspaceCreated.class)
    Workspace createWorkspace(WorkspaceKey key, Profile profile);
    List<Workspace> listWorkspaces() throws WorkspaceControllerException;
    Optional<Workspace> getByKey(WorkspaceKey key) throws WorkspaceControllerException;

}
